<div class="col">
    <div class="d-flex flex-row bd-highlight mb-3">
        <div class="p-2 bd-highlight mt-5">
            <img src="./konten/Radtek.png" width="200" alt="">
        </div>
    </div>

</div>
<div class="wrapper">
    <div class="tittle mt-3">
        <h2>Login</h2>
    </div>
    <div class="field">
        <label for="" style="padding-top: 13px">Email</label>
        <input class="rounded" type="text" placeholder="masukkan email" id="enail" name="email">
    </div>
    <div class="form-border"></div>
    <div class="field">
        <label for="">password</label>
        <input class="rounded" type="password" placeholder="masukkan password" id="password" name="password">
    </div>
    <div class="form-border"></div>
    <div class="content">
        <div class="checkbox mx-3">
            <input class="rounded" type="checkbox" id="rememberMe">
            <label for="rememberMe">Remember Me</label>
        </div>
        <div class="pass-link mx-2">
            <a href="#">forgot?</a>
        </div>
    </div>
    <button class="btn btn-primary rounded-pill" type="submit" value="login">Login</button>
    <div class="signup-link">
        <a href="#">Daftar Baru</a>
    </div>
</div>