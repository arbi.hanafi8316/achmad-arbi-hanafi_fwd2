<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <a class="navbar-brand ml-5" href="#">
        <img src="<?= base_url('assets/'); ?>./konten/Radtek.png" width="100" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Promo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Konsultasi</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button class="btn background-neon my-2 mx-2 my-sm-0 border border-dark rounded-pill" type="button" onclick="window.location.href='<?= base_url('auth/login'); ?>'">Login</button>
            <button class="btn background-white my-2 my-sm-0 border border-dark rounded-pill" type="submit">Daftar</button>
        </form>
    </div>
</nav>
<!-- end navbar -->
<!-- landing page 1 -->
<section class="jumbotron px-1 py-1">
    <div class="landingpage1">
        <div class="row align-items-center">
            <div class="col-sm ml-5 pl-5">
                <h1>Buat Website Harga Murah dan Terjangkau Kualitasnya</h1>
                <h5>PoshWeb layanan jasa pembuatan website dan hosting, dengan 45 ribu serta promo yang kami punya anda dapat memiliki website anda sendiri</h5>
                <a class="btn btn-primary bg-blue border border-dark rounded-pill mt-3" href="#" role="button">Order</a>
            </div>
            <div class="col-sm">
                <img src="<?= base_url('assets/'); ?>./konten/Desain_tanpa_judul-removebg-preview.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="information ml-5 my-0">
            <div class="d-flex justify-content-start">
                <div class="p-2 bd-highlight mx-4 text-white">
                    <h5><i class="fa-solid fa-phone"></i> Contact Us</h5>
                    <p>+62-81xxxxxxxx</p>
                </div>
                <div class="p-2 bd-highlight mx-4 text-white">
                    <h5><i class="fa-solid fa-building"></i> Alamat</h5>
                    <p>Jl. Sukarno Hatta 150-151</p>
                </div>
                <div class="p-2 bd-highlight mx-4 text-white">
                    <h5><i class="fa-brands fa-instagram"></i> Instagram</h5>
                    <p>@PoshWeb</p>
                </div>
                <div class="p-2 bd-highlight text-white">
                    <h5><i class="fa-brands fa-linkedin"></i> Linked-In</h5>
                    <p>@PoshWeb</p>
                </div>

            </div>

        </div>
        <div class="arrow">
            <a href="#" class="tombol rounded-circle border border-dark"><i class="fa-solid fa-arrow-down"></i></a>
        </div>
    </div>
</section>
<!-- end landing page 1 -->
<!-- landing page 2 -->
<section class="landingpage2">
    <div class="container">
        <div class="d-flex flex-column align-items-center">
            <h3>Website Yang Anda Terima Memiliki :</h3>
            <div class="container">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-blueyoung px-3 rounded align-items-center">
                            <img src="<?= base_url('assets/'); ?>./konten/brain.svg" width="100" alt="">
                            <div class="media-body py-2">
                                <h4 class="py-3 text-center">Teknologi Terbaru</h4>
                                <p class="text-center align-justify-center">Teknologi terbaru yang digunakan akan membantu mempercepat dan meningkatkan performa website anda</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-bluewhite px-3 rounded align-items-center">
                            <img src="<?= base_url('assets/'); ?>./konten/free.svg" width="100" alt="">
                            <div class="media-body py-2">
                                <h4 class="py-3 text-center">Free Domain</h4>
                                <p class="text-center align-justify-center">Domain yang digunakan merupakan domain hits, sehingga lebih singkat dan relevan untuk seluruh website</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-blueyoung px-3 rounded align-items-center">
                            <img src="<?= base_url('assets/'); ?>./konten/komputer.svg" width="79" alt="">
                            <div class="media-body py-2">
                                <h4 class="py-3 text-center">Mudah Dikelola</h4>
                                <p class="text-center align-justify-center">Anda tidak perlu khawatir tentang pengelolaan karena kami menggunakan engine wordpress sehingga mudah dikelola</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="container">
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="media flex-column border border-dark bg-blueyoung px-3 rounded align-items-center">
                            <img src="<?= base_url('assets/'); ?>./konten/responsive.svg" width="100" alt="">
                            <div class="media-body py-2">
                                <h4 class="py-3 text-center">Responsive</h4>
                                <p class="text-center align-justify-center">Website yang anda punya memiliki tingkat responsive yang tinggi, hingga seluruh perangkat dapat mengakses website anda</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="media flex-column border border-dark bg-blueyoung px-3 rounded align-items-center">
                            <img src="<?= base_url('assets/'); ?>./konten/medali.svg" width="100" alt="">
                            <div class="media-body py-2">
                                <h4 class="py-3 text-center">Tampilan yang Juara</h4>
                                <p class="text-center align-justify-center">Tampilan yang kami berikan mampu memberikan kesan dan pesan tersampaikan dan tidak memiliki kesamaan dengan website lain</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end landing page 2 -->
<br>
<hr>
<!-- landing page 3 -->
<section class="landingpage3">
    <div class="container">
        <div class="d-flex flex-column align-items-center">
            <h3>Promo Kami</h3>
            <p>09 - 30 September 2022</p>
            <div class="container">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-blueyoung py-4 rounded align-items-center">
                            <h4 class="py-1">Promo Diskon 15%</h4>
                            <img src="<?= base_url('assets/'); ?>./konten/angkasa5.jpg" width="338" alt="">
                            <br>
                            <br>
                            <div class="media-body">
                                <a href="#" class="btn btn-primary bg-neon">Order</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-blueyoung py-4 rounded align-items-center">
                            <h4 class="py-1">Promo Diskon 20%</h4>
                            <img src="<?= base_url('assets/'); ?>./konten/angkasa6.jpg" width="338" alt="">
                            <br>
                            <br>
                            <div class="media-body">
                                <a href="#" class="btn btn-primary bg-neon">Order</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media flex-column border border-dark bg-blueyoung py-4 rounded align-items-center">
                            <h4 class="py-1">Promo Diskon 22%</h4>
                            <img src="<?= base_url('assets/'); ?>./konten/angkasa7.jpg" width="338" alt="">
                            <br>
                            <br>
                            <div class="media-body">
                                <a href="#" class="btn btn-primary bg-neon">Order</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lingkaran1"></div>
            <div class="container mt-3">
                <h3>Support Payment</h3>
                <div class="d-flex flex-row bd-highlight mb-3 ">
                    <div class="p-2 bd-highlight">
                        <img src="<?= base_url('assets/'); ?>./konten/bca.svg" width="100" alt="">
                    </div>
                    <div class="p-2 bd-highlight ml-3 my-3">
                        <img src="<?= base_url('assets/'); ?>./konten/gopay.svg" width="100" alt="">
                    </div>
                    <div class="p-2 bd-highlight ml-3 my-4">
                        <img src="<?= base_url('assets/'); ?>./konten/Logo_Indomaret.svg" width="100" alt="">
                    </div>
                    <div class="p-2 bd-highlight ml-3 my-3">
                        <img src="<?= base_url('assets/'); ?>./konten/ovo.svg" width="100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end landing page 3 -->
<hr>
<!-- landing page 4 -->
<section class="landingpage4">
    <div class="container">
        <div class="d-flex flex-column align-items-center">
            <div class="container">
                <div class="row mt-5">

                    <div class="rectangle col-md-3 rounded-2">
                        <div class="media flex-column border border-dark bg-yellow py-4 rounded align-items-center">
                            <h5 class="py-1">Adi Setiawan</h4>
                                <img src="<?= base_url('assets/'); ?>./konten/orang4.jpg" width="50" class="" alt="">
                                <div class="media-body px-2 py-1">
                                    <p class="px-3">“PoshWeb membantu saya untuk membuat web untuk sekolah dan respon siswa sangat suka akan tampilan yang diberikan”</p>
                                </div>
                        </div>
                    </div>
                    <div class="rectangle col-sm-5 rounded-5">
                        <div class="media flex-column border border-dark bg-yellow py-4 rounded align-items-center">
                            <h5 class="py-1">Sandika Galuh</h4>
                                <img src="<?= base_url('assets/'); ?>./konten/orang2.jpg" width="50" class="" alt="">
                                <div class="media-body px-3 py-1">
                                    <p>“PoshWeb memberikan promo yang berkualitas, pelayanan yang diberikan sangat baik, membantu client unruk meminimalisir downtime, dan yang paling saya suka adalah pelayanan support CSnya sangat mantab, apalagi tampilan yang diberikan”</p>
                                </div>
                        </div>
                    </div>

                    <!-- Force next columns to break to new line -->
                    <div class="w-100"></div>

                    <div class="rectangle col-md-4 mt-3 rounded-2">
                        <div class="media flex-column border border-dark bg-yellow py-4 rounded align-items-center">
                            <h5 class="py-1">Putra Cahyanti</h4>
                                <img src="<?= base_url('assets/'); ?>./konten/orang3.jpg" width="70" class="" alt="">
                                <div class="media-body py-1">
                                    <p class="px-3">“Sampai saat ini cukup puas karena promo yang diberikan PoshWeb sangat mantab”</p>
                                </div>
                        </div>
                    </div>
                    <div class="rectangle col-sm-3 mt-3 rounded-2">
                        <div class="media flex-column border border-dark bg-yellow py-4 rounded align-items-center">
                            <h5 class="py-1">M.Ilham Manaf</h4>
                                <img src="<?= base_url('assets/'); ?>./konten/orang1.jpg" width="70" class="" alt="">
                                <div class="media-body px-3 py-1">
                                    <p>“Dengan bantuan PoshWeb saya sangat terbantu karena pelayanan yang responsif”</p>

                                </div>
                        </div>
                    </div>
                    <div class="judul col-md px-1">
                        <h2>Pendapat Mereka Tentang Kami?</h2>
                    </div>
                </div>
                <div class="slide d-flex">
                    <div class="slide1 rounded-circle mx-2">
                        <a href="#"></a>
                    </div>
                    <div class="slide2 rounded-circle mx-3">
                        <a href="#"></a>
                    </div>
                    <div class="slide3 rounded-pill">
                        <a href="#"></a>
                    </div>
                </div>
                <div class="perusahaan d-flex px-5">
                    <h3>-Lebih dari 70.000 perusahaan percaya pada kami-</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end landing page 4 -->